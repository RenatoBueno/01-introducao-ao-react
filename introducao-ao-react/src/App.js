import React, { Component } from 'react'
import logo from './logo.svg'
import './App.css'

class App extends Component {
	render() {
		const data = new Date()
		return (
			<div className="Center">
				<h1>hello world</h1>
				<p>{data.toDateString()}</p>
				<p className="P1">hello world</p>
				<p>{data.toDateString()}</p>
				<p className="P2">hello world</p>
				<p>{data.toDateString()}</p>
				<p className="P3">hello world</p>
				<p className="Ph1">{data.toDateString()}</p>
				<p className="Ph2">{data.toDateString()}</p>
				<p className="P4">hello world</p>
				<p className="P5">hello world</p>
				<p className="Ph3">{data.toDateString()}</p>
				<p className="P6">hello world</p>
				<p>{data.toDateString()}</p>
				<p className="P7">hello world</p>
				<p>{data.toDateString()}</p>
				<p className="P8">
					hello world
					<br /> <br />
					{data.toDateString()}
				</p>
				<p className="P9">
					hello world
					<br /> <br />
					{data.toDateString()}
				</p>
			</div>
		)
	}
}

export default App
